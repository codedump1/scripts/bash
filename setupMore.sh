#/bin/bash
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" #installs oh-my-zsh
git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d #downloads doom emacs
~/.emacs.d/bin/doom install #installs doom emacs
curl -kL https://goo.gl/Vu8qGR | bash #installs my music player/streamer
echo done
